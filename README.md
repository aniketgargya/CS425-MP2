# 425-MP2
Gossip-style heartbeats

To start the introducer node:
```
go run cmd/peer/main.go --machine_name <machine_name> --hostname <hostname> --port <port_number> --drop_rate <drop_rate>
```

To start other nodes:
```
go run cmd/peer/main.go --machine_name <machine_name> --hostname <hostname> --port <port_number> --drop_rate <drop_rate> --introducer <introducer_hostname:introducer_port_number>
```

The drop rate should be a number between 0.0 and 1.0, but if not specified, the default value will be used (0.0).

Once the program is running, the following command can be used to interact with the system:
- `leave`: Leave the networking protocol
- `list_mem`: List the current membership list
- `list_self`: List the current node's id
- `enable_suspicion` or `disable_suspicion`: Enable or disable the suspicion protocol, Note: this may only be called by the introducer node
- `drop_rate <drop_rate>`: Change the drop rate to `<drop_rate>`
