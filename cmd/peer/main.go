package main;

import (
	"fmt"
    "net"
    "time"
    "flag"
	pb "github.com/mjacob1002/425-MP2/pkg/gen_proto"
	ni "github.com/mjacob1002/425-MP2/pkg/node_info"
	"github.com/mjacob1002/425-MP2/pkg/utils"
    "google.golang.org/protobuf/proto"
	"math/rand"
    "strconv"
    "log"
    "os"
)

func shouldDropMessage() (bool) {
	randomFloat := rand.Float64()
	ni.DropRateLock.Lock()
	should_drop := (randomFloat < ni.DropRate)
	ni.DropRateLock.Unlock()
	return should_drop
}

func processHeartbeat(heartbeat *pb.HeartbeatMessage) {
    ni.Lock.Lock()
    defer ni.Lock.Unlock()

    plusSSynced := ni.PlusS == heartbeat.PlusS

    if heartbeat.ProtocolUpdateHeartbeatCounter > ni.ProtocolUpdateHeartbeatCounter {
        ni.PlusS = heartbeat.PlusS
        ni.ProtocolUpdateHeartbeatCounter = heartbeat.ProtocolUpdateHeartbeatCounter
        if ni.PlusS {
            log.Println("Enabled suspicion protocol")
        } else {
            log.Println("Disabled suspicion protocol")
        }
    }

    for _, entry := range heartbeat.Table.Entries {
        if entry.MachineId == ni.ThisMachineId {
            if entry.State == pb.SUSPICION_STATE_DEAD {
                log.Println("Current node is considered dead, leaving")
                os.Exit(0)
            } else if entry.State == pb.SUSPICION_STATE_SUSPECTED {
                updatedEntry := ni.MembershipList[ni.ThisMachineId]
                updatedEntry.IncarnationNumber = updatedEntry.IncarnationNumber + 1
                updatedEntry.State = pb.SUSPICION_STATE_ALIVE
                log.Println("Current node is considered suspicious, incrementing incarnation number")
            }
            continue
        }

        if _, ok := ni.DeadMembers[entry.MachineId]; ok {
            continue
        }

        if _, ok := ni.MembershipList[entry.MachineId]; !ok {
            log.Println("Adding new node to membership list:", entry.MachineId)
            newEntry := pb.TableEntry{}
            newEntry.MachineId = entry.MachineId
            newEntry.HeartbeatCounter = entry.HeartbeatCounter
            newEntry.Hostname = entry.Hostname
            newEntry.Port = entry.Port
            newEntry.LocalTime = time.Now().Unix()
            newEntry.IncarnationNumber = entry.IncarnationNumber
            newEntry.State = entry.State
            newEntry.StateTime = time.Now().Unix()
            ni.MembershipList[newEntry.MachineId] = newEntry
        } else if entry.HeartbeatCounter > ni.MembershipList[entry.MachineId].HeartbeatCounter {
            updatedEntry := ni.MembershipList[entry.MachineId]
            updatedEntry.HeartbeatCounter = entry.HeartbeatCounter
            updatedEntry.LocalTime = time.Now().Unix()


            if ni.PlusS && plusSSynced {
                if (entry.State == pb.SUSPICION_STATE_DEAD && updatedEntry.State != entry.State) ||
                    (entry.IncarnationNumber > updatedEntry.IncarnationNumber) ||
                    (entry.IncarnationNumber == updatedEntry.IncarnationNumber && entry.State == pb.SUSPICION_STATE_SUSPECTED && updatedEntry.State == pb.SUSPICION_STATE_ALIVE) {

                    updatedEntry.State = entry.State
                    updatedEntry.IncarnationNumber = entry.IncarnationNumber
                    updatedEntry.StateTime = time.Now().Unix()

                    if entry.State == pb.SUSPICION_STATE_ALIVE {
                        log.Println("Node is now considered alive:", updatedEntry.MachineId)
                    } else if entry.State == pb.SUSPICION_STATE_SUSPECTED {
                        log.Println("Node is now considered suspicious:", updatedEntry.MachineId)
                        fmt.Println(time.Now().Unix(), "Node is now considered suspicious:", updatedEntry.MachineId)
                    } else if entry.State == pb.SUSPICION_STATE_DEAD {
                        log.Println("Node is now considered dead:", updatedEntry.MachineId)
                    }
                }
            } else if !ni.PlusS {
                updatedEntry.State = pb.SUSPICION_STATE_ALIVE
                updatedEntry.StateTime = time.Now().Unix()
            }

            ni.MembershipList[updatedEntry.MachineId] = updatedEntry
        }
    }
}

func sendPeriodicHeartbeats() {
    for {
        time.Sleep(250 * time.Millisecond)

        utils.SendOutHeartbeats()
        utils.CleanupTable()
    }
}

func periodicCleanupTable() {
    for {
        time.Sleep(500 * time.Millisecond)

    }
}

func listenInitializer() {
    udpAddress, err := net.ResolveUDPAddr("udp", ":" + ni.ThisPort)
    if err != nil {
        fmt.Println(err)
        return
    }

    conn, err := net.ListenUDP("udp", udpAddress)
    if err != nil {
        fmt.Println(err)
        return
    }

    defer conn.Close()

    buffer := make([]byte, 65535)

    for {
        n, _, err := conn.ReadFromUDP(buffer)
        if err != nil {
            fmt.Println(err)
        }

        message := &pb.HeartbeatMessage{}
		if shouldDropMessage() {
			continue
		}

        if err := proto.Unmarshal(buffer[0:n], message); err != nil {
            fmt.Println("Error unmarshaling protobuf data:", err)
            return
        }

        processHeartbeat(message)
    }
}

func main(){
    // parse arguments
    flag.StringVar(&ni.ThisMachineName, "machine_name", "", "Machine Name")
    flag.StringVar(&ni.ThisHostname, "hostname", "", "Hostname")
    flag.StringVar(&ni.ThisPort, "port", "", "Port")
    flag.StringVar(&ni.Introducer, "introducer", "", "Introducer Node Address")
	flag.Float64Var(&ni.DropRate, "drop_rate", 0.00, "Drop rate of packets");
    flag.BoolVar(&ni.PlusS, "S", false, "Enable suspicion protocol")

    flag.Parse()

    if ni.PlusS && ni.Introducer != "" {
        log.Fatal("Suspicion can only be introduced by the introducer")
        return
    }

    logFile, err := os.Create("machine." + ni.ThisMachineName + ".log")

    if err != nil {
        log.Fatal("Failed to create log file:", err)
    }
    defer logFile.Close()

    log.SetOutput(logFile)

    if ni.PlusS {
        ni.PlusS = true
        ni.ProtocolUpdateHeartbeatCounter = 0
        log.Println("Enabled suspicion protocol")
    }

    ni.ThisMachineId = ni.ThisMachineName + "_" + strconv.FormatInt(time.Now().Unix(), 10)

    fmt.Println("Node Info:", ni.ThisMachineId, ni.ThisHostname, ni.ThisPort, ni.Introducer)

    // register myself to initializer node
    ni.ThisNode = pb.TableEntry {
        MachineId: ni.ThisMachineId,
        HeartbeatCounter: 0,
        Hostname: ni.ThisHostname,
        Port: ni.ThisPort,
        LocalTime: time.Now().Unix(),
        IncarnationNumber: 0,
        State: pb.SUSPICION_STATE_ALIVE,
        StateTime: 0,
    }

    ni.MembershipList[ni.ThisNode.MachineId] = ni.ThisNode

     if ni.Introducer != "" {
		utils.SendHeartbeatAddress(ni.Introducer)
    }

    go listenInitializer()
    go sendPeriodicHeartbeats()
    // go periodicCleanupTable()
	go utils.ListenToCommands()


    select {}
}
