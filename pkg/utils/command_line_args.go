package utils;

import (
	"bufio"
	"fmt"
	"os"
    "log"
	ni "github.com/mjacob1002/425-MP2/pkg/node_info"
	pb "github.com/mjacob1002/425-MP2/pkg/gen_proto"
	"strings"
	"strconv"
)

func ListenToCommands(){
	reader := bufio.NewReader(os.Stdin)
	for {
		text, _ := reader.ReadString('\n')
		if text == "leave\n" {
			os.Exit(0);
		} else if text == "list_mem\n" {
			fmt.Println("printing membership list of ", ni.ThisMachineId)
			ni.Lock.Lock();
			for key, value := range ni.MembershipList {
                if !ni.PlusS || value.State != pb.SUSPICION_STATE_DEAD  {
                    fmt.Printf("- %v\n", key)
                }
			}
			ni.Lock.Unlock()
		} else if text == "list_self\n" {
			fmt.Printf("My node ID is %s\n", ni.ThisMachineId)
		} else if text == "enable_suspicion\n" || text == "disable_suspicion\n" {
            newPlusS := text == "enable_suspicion\n"

            if ni.Introducer != "" {
                fmt.Println("Suspicion can only be enabled/disabled by introducer", ni.Introducer)
            } else if ni.PlusS != newPlusS {
                ni.PlusS = newPlusS
                ni.ProtocolUpdateHeartbeatCounter = ni.MembershipList[ni.ThisMachineId].HeartbeatCounter;
                if ni.PlusS {
                    log.Println("Enabled suspicion protocol")
                } else {
                    log.Println("Disabled suspicion protocol")
                }
            }
	} else if len(text) > len("drop_rate ") && text[0:len("drop_rate ")] == "drop_rate " {
		drop_rate := strings.Split(text, " ")[1]
		newDropRate, err := strconv.ParseFloat(drop_rate, 64)
		if err != nil{
			fmt.Println("Something is wrong when trying to set new drop rate: ", err)
			continue;
		} else {
			ni.DropRateLock.Lock();
			ni.DropRate = newDropRate;
			fmt.Printf("New drop rate set to %f\n", ni.DropRate); 
			ni.DropRateLock.Unlock()
		}
	}else {
			fmt.Println(text, " is not a good command to use; try again");
		}
	}
}
