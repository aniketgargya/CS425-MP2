package utils;

import(
	"fmt"
	"net"
    "math/rand"
    "time"
	pb "github.com/mjacob1002/425-MP2/pkg/gen_proto"
	ni "github.com/mjacob1002/425-MP2/pkg/node_info"
    "google.golang.org/protobuf/proto"
    "log"
)

func SendOutHeartbeats() {
    ni.Lock.Lock()
    defer ni.Lock.Unlock()
    keys := make([]string, 0, len(ni.MembershipList))
	for key := range ni.MembershipList {
		keys = append(keys, key)
	}

	for i := len(keys) - 1; i > 0; i-- {
		j := rand.Intn(i + 1)
		keys[i], keys[j] = keys[j], keys[i]
	}

    k := 5

    if len(ni.MembershipList) < k {
        k = len(ni.MembershipList)
    }

	targetMachineIds := keys[:k]

    // fmt.Println("sending to:", targetMachineIds)

    IncrementHeartbeat()
    for _, targetMachineId := range targetMachineIds {
        if targetMachineId == ni.ThisMachineId {
            continue
        }

        SendHeartbeat(ni.MembershipList[targetMachineId].Hostname, ni.MembershipList[targetMachineId].Port)
    }
}

func CleanupTable() {
    ni.Lock.Lock()
    defer ni.Lock.Unlock()
    for key, value := range ni.MembershipList {
        if key == ni.ThisMachineId {
            continue
        }

        if ni.PlusS {
            if value.State == pb.SUSPICION_STATE_ALIVE {
                if time.Now().Unix() - value.LocalTime >= 2 {
                    value.State = pb.SUSPICION_STATE_SUSPECTED 
                    value.StateTime = time.Now().Unix()
                    log.Println("Node is now considered suspicious:", value.MachineId)
                    fmt.Println(time.Now().Unix(), "Node is now considered suspicious:", value.MachineId)
                    ni.MembershipList[key] = value
                }
            } else if value.State == pb.SUSPICION_STATE_SUSPECTED {
                if time.Now().Unix() - value.StateTime >= 3 {
                    value.State = pb.SUSPICION_STATE_DEAD
                    value.StateTime = time.Now().Unix()
                    log.Println("Node is now considered dead:", value.MachineId)
                    ni.MembershipList[key] = value
                }
            } else if value.State == pb.SUSPICION_STATE_DEAD {
                if time.Now().Unix() - value.StateTime >= 10 {
                    delete(ni.MembershipList, key)
                }
            }
        } else {
            var tfail int64 = 5
            if time.Now().Unix() - value.LocalTime >= tfail {
                ni.DeadMembers[key] = value.LocalTime
                log.Println("Deleting node from membership list:", key)
                delete(ni.MembershipList, key)
            }
        }
    }
}

func MakeMembershipListArray(membershipList map[string]pb.TableEntry) ([]*pb.TableEntry){
		membershipListArray := make([]*pb.TableEntry, 0, len(ni.MembershipList))
		for _, value := range ni.MembershipList {
            copiedValue := value
			membershipListArray = append(membershipListArray, &copiedValue)
		}
		return membershipListArray
}

func IncrementHeartbeat(){
		entry, ok := ni.MembershipList[ni.ThisMachineId];
		if ok == false {
			fmt.Println("We do not exist in our own membership list!")
			return
		}
		entry.HeartbeatCounter = entry.HeartbeatCounter + 1
		ni.MembershipList[ni.ThisMachineId] = entry
}

func MakeHeartbeatFromMembershipList(membershipList map[string]pb.TableEntry)(pb.HeartbeatMessage){
		membershipArray := MakeMembershipListArray(ni.MembershipList)
		membershipTable := pb.Table{ Entries : membershipArray}
		heartbeatMessage := pb.HeartbeatMessage{
            Table: &membershipTable,
            ProtocolUpdateHeartbeatCounter: ni.ProtocolUpdateHeartbeatCounter,
            PlusS: ni.PlusS,
        }
		return heartbeatMessage
}

func SendHeartbeat(hostname string, port string){
    SendHeartbeatAddress(hostname + ":" + port)
}

func SendHeartbeatAddress(address string){
	udpAddr, err := net.ResolveUDPAddr("udp", address)
	if err != nil {
		fmt.Println(err)
		return
	}
	conn, err := net.DialUDP("udp", nil, udpAddr)
	if err != nil{
		fmt.Println(err)
		return
	}
	defer conn.Close()
	// create the hearbeat
	heartbeat := MakeHeartbeatFromMembershipList(ni.MembershipList)
	heartbeat_serialized, err := proto.Marshal(&heartbeat)
	if err != nil{
		fmt.Println(err)
		return
	}
	_, err = conn.Write(heartbeat_serialized)
	if err != nil{
		fmt.Println("error sending UDP packet: ", err)
		return
	}
}

