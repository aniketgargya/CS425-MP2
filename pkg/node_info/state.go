package node_info;

import(
	pb "github.com/mjacob1002/425-MP2/pkg/gen_proto"
    "sync"
)

// global state of the node is stored here
var MembershipList = make(map[string]pb.TableEntry)
var ThisMachineName string
var ThisMachineId string
var ThisHostname string
var ThisPort string
var Introducer string
var ThisNode pb.TableEntry
var Lock sync.Mutex
var DropRate float64
var DropRateLock sync.Mutex
var DeadMembers = make(map[string]int64)
var ProtocolUpdateHeartbeatCounter int32 = -1
var PlusS bool = false
